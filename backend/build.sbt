name := "backend"

version := "1.0"

scalaVersion := "2.11.6"

resolvers += "krasserm at bintray" at "http://dl.bintray.com/krasserm/maven"

libraryDependencies ++= {
  val akkaV = "2.4.14"
  val akkaHttpV = "10.0.0"
  val springDataVersion = "2.0.0.M1"
  val scalaTestV = "3.0.1"


  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-stream" % akkaV,
    "com.typesafe.akka" %% "akka-testkit" % akkaV,
    "com.typesafe.akka" %% "akka-http" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpV,
    "com.typesafe.akka" %% "akka-persistence" % akkaV,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpV,
    "org.scalatest" %% "scalatest" % scalaTestV % "test",
    "com.softwaremill.macwire" %% "macros" % "2.2.5" % "provided",
    "com.softwaremill.macwire" %% "util" % "2.2.5",
    "com.softwaremill.macwire" %% "proxy" % "2.2.5",
    "com.github.krasserm" %% "akka-persistence-cassandra-3x" % "0.6",
    "com.miguno.akka" %% "akka-mock-scheduler" % "0.5.0",
    "org.dmonix.akka" %% "akka-persistence-mock" % "1.1.1",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.4.0",
    "com.github.krasserm" %% "akka-persistence-kafka" % "0.4",
    "org.apache.kafka" % "kafka-clients" % "0.10.1.0",
    "org.json4s" %% "json4s-native" % "3.3.0",
    "org.scaldi" %% "scaldi-akka" % "0.5.8"

  )
}