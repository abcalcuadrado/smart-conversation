package com.smartchat.microservice.chat.domain.aggregate

/**
  * Created by xavier on 12/13/16.
  */

import java.util.UUID
import java.util.concurrent.atomic.AtomicInteger

import akka.actor.{ActorRef, ActorSystem, Props, Scheduler}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest._
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}

import scala.concurrent.duration._
import akka.pattern.ask
import akka.util.Timeout
import com.smartchat.microservice.chat.domain.aggregate.Conversation.{AppendBuyerMessageCmd, AppendSellerMessageCmd, CreateConversationCmd, IsCreated}
import org.scalatest.concurrent.ScalaFutures

import scala.concurrent.ExecutionContext.Implicits.global
import com.miguno.akka.testing.VirtualTime
import com.smartchat.microservice.sharedkernel.application.eventhandler.EventHandler
import com.smartchat.microservice.sharedkernel.util.{Error, Ok}
import com.smartchat.microservice.util.PersistenceSuite

class ConversationTest extends TestKit(ActorSystem(
  "TestKitUsageSpec", PersistenceSuite.config)) with ImplicitSender with Matchers with FunSpecLike with ScalaFutures with BeforeAndAfter {
  implicit val timeout = Timeout(15 seconds)

  var time: VirtualTime = _
  var scheduler: Scheduler = _
  var conversation: ActorRef = _
  var eventSubscriber: ActorRef = _


  before {
    time = new VirtualTime()
    scheduler = time.scheduler
    conversation = system.actorOf(Props(classOf[Conversation], UUID.randomUUID().toString, scheduler))
    eventSubscriber = system.actorOf(Props(classOf[EventHandler]))
  }
  describe("Conversation") {

    it("SHOULD return false WHEN ask IsCreated and state is not created yet") {
      val future = (conversation ? IsCreated()).map(_.asInstanceOf[Boolean])
      whenReady(future, timeout(6 seconds)) { result =>
        result should be(false)
      }
    }

    it("SHOULD return NOT_CREATE_YET WHEN some a first cmd different of CreateConversationCmd is sent") {
      val future = conversation ? ""
      whenReady(future, timeout(6 seconds)) {
        case Error(Conversation.NOT_CREATED_YET) => assert(true)
        case _ => assert(false)
      }
    }

    it("SHOULD return true WHEN ask IsCreated and conversation was created before") {
      conversation ! CreateConversationCmd("1", "someUrl1", "2", "someUrl2")
      val future = (conversation ? IsCreated()).map(_.asInstanceOf[Boolean])
      whenReady(future, timeout(6 seconds)) { result =>
        result should be(true)
      }
    }


    it("SHOULD accept just one last cmd from seller WHEN count down is out") {
      conversation ! CreateConversationCmd("1", "someUrl1", "2", "someUrl2")
      val future = conversation ? AppendBuyerMessageCmd("this is a message", 1)
      val result = future.flatMap(_ => {
        time.advance(1.day)
        conversation ? AppendSellerMessageCmd("this is a message", 1)
        conversation ? AppendSellerMessageCmd("this is a message", 1)

      })
      whenReady(result, timeout(6 seconds)) {
        case Error(Conversation.SELLER_MESSAGES_NOT_ALLOW) =>
          assert(true)
        case _ => assert(false)
      }


    }


  }

}
