package com.smartchat.microservice.chat.rest.v1

/**
  * Created by xavier on 12/13/16.
  */

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.util.ByteString
import com.smartchat.microservice.Main
import org.scalatest.{Matchers, WordSpec}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.server._
import Directives._

class FacebookWebhookControllerTest extends WordSpec with Matchers with ScalatestRouteTest {

  val controllerRoute = Main.route


  "The service" should {

    "return a greeting for GET requests to the root path" in {
      // tests:

      Post("/chat/v1/facebook/22", HttpEntity(ContentTypes.`application/json`, data = ByteString(
        """{
              "object": "page",
              "entry": [
                  {
                      "id": "211625915956065",
                      "time": 1481681114045,
                      "messaging": [
                          {
                              "sender": {
                                  "id": "1663274070365496"
                              },
                              "recipient": {
                                  "id": "211625915956065"
                              },
                              "timestamp": 1481681113981,
                              "message": {
                                  "mid": "mid.1481681113981:3fd1d1bf49",
                                  "seq": 3,
                                  "text": "holaaaa"
                              }
                          }
                      ]
                  }
              ]
          }"""))) ~> controllerRoute ~> check {
        responseAs[String] shouldEqual "hola que ase"
      }
    }
  }

}
