package com.smartchat.microservice

import com.typesafe.config.ConfigFactory

/**
  * Created by jorge on 17/12/16.
  */
object Config {
  protected val config = ConfigFactory.load()
  val kafkaConfig = config.getConfig("kafka")
  val topicName = kafkaConfig.getString("kafka.topic.name")
  val topicPartitionsNumber = kafkaConfig.getInt("kafka.topic.partitions.number")
}
