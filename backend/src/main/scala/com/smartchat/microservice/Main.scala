package com.smartchat.microservice

/**
  * Created by xavier on 11/29/16.
  */

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.smartchat.microservice.chat.DIModule
import com.smartchat.microservice.chat.rest.MainController
import scaldi.Module
import scaldi.akka.AkkaInjectable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.io.StdIn


class AkkaModule extends Module {
  implicit val system = ActorSystem("ActorSystem")
  bind[ActorSystem] to system destroyWith (_.terminate())
  bind[ActorMaterializer] to ActorMaterializer()
}

object Main extends AkkaInjectable {
  implicit val appModule = new AkkaModule :: new DIModule

  def main(args: Array[String]) {
    implicit val system = inject[ActorSystem]
    implicit val materializer = inject[ActorMaterializer]
    val chatMainController = inject[MainController]
    //    implicit val executionContext = system.dispatcher


    val bindingFuture = Http().bindAndHandle(chatMainController.route, "localhost", 8080)
    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ ⇒ system.terminate()) // and shutdown when done

  }
}