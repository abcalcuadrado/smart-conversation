package com.smartchat.microservice.sharedkernel.domain.aggregate

import akka.persistence.PersistentActor
import com.smartchat.microservice.sharedkernel.domain.aggregate.AggregateRoot.Evt

/**
  * Created by xavier on 12/16/16.
  */

object AggregateRoot {

  trait Evt

  case class IsCreatedCmd()

}

abstract class AggregateRoot extends PersistentActor {
  override def persist[A](event: A)(handler: (A) => Unit): Unit = {

    context.system.eventStream.publish(event.asInstanceOf[Object])
    super.persist(event)(handler)
  }






}
