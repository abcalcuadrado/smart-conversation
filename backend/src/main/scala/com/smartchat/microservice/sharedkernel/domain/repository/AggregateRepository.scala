package com.smartchat.microservice.sharedkernel.domain.repository

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorRef, Props}
import com.smartchat.microservice.sharedkernel.domain.aggregate.AggregateRoot.IsCreatedCmd
import com.smartchat.microservice.sharedkernel.domain.manager.AggregateManager
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import com.smartchat.microservice.sharedkernel.domain.repository.AggregateRepository.FindById

import scala.concurrent.Future
import scala.reflect.ClassTag
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by xavier on 12/21/16.
  */
object AggregateRepository {

  case class FindById(id: String)

}


class AggregateRepository[T <: AggregateManager : ClassTag] extends Actor {
  val aggregateManager = context.actorOf(Props[T])
  implicit val timeout = Timeout(1 seconds)

  override def receive: Receive = {
    case FindById(id) => (aggregateManager ? id).asInstanceOf[Future[ActorRef]].map(actorRef => {
      actorRef ? IsCreatedCmd() map {
        case true => sender ! Some(actorRef)
        case _ => sender ! None
      }
    })
  }
}
