package com.smartchat.microservice.sharedkernel.application.eventhandler

import akka.actor.Actor
import akka.actor.Actor.Receive
import com.smartchat.microservice.chat.domain.aggregate.Conversation.ConversationCreatedEvt
import com.smartchat.microservice.sharedkernel.domain.aggregate.AggregateRoot.Evt

/**
  * Created by xavier on 12/16/16.
  */
abstract class EventHandler extends Actor {

  override def preStart = context.system.eventStream.subscribe(self, classOf[Any])

}
