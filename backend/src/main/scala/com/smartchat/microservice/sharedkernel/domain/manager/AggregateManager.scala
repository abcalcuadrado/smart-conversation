package com.smartchat.microservice.sharedkernel.domain.manager

import akka.actor.{Actor, ActorSystem, Props}
import com.smartchat.microservice.chat.domain.aggregate.Conversation
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global


/**
  * Created by xavier on 12/14/16.
  */
abstract class AggregateManager extends Actor {
  implicit val timeout = Timeout(1 seconds)

  val propsFactory: String => Props

  override def receive: Receive = {
    case id: String => context.actorSelection(id).resolveOne().onComplete {
      case Success(actor) => sender() ! actor
      case Failure(ex) =>
        println(id, "=====")
        sender() ! context.actorOf(propsFactory(id), id)
    }
  }
}
