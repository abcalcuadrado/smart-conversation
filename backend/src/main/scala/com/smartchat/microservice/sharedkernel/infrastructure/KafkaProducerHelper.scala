package com.smartchat.microservice.sharedkernel.infrastructure


import java.util.UUID

import com.smartchat.microservice.Config
import com.smartchat.microservice.sharedkernel.domain.aggregate.AggregateRoot.Evt
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import com.smartchat.microservice.util.ConfigUtil.propsFromConfig
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.json4s.DefaultFormats
import org.json4s.native.Serialization.write

object KafkaProducerHelper {
  protected lazy val logger: Logger = Logger(LoggerFactory.getLogger(getClass.getName))
  val kafkaProducer  = new KafkaProducer[String, String](propsFromConfig(Config.kafkaConfig))
  val topicPartitionsNumber = Config.topicPartitionsNumber
  val topicName = Config.topicName

  def calculatePartitionIdByAggregateIdAndTotalPartitions(aggregateId: String, totalPartitions : Int = 1): Int ={
    val uuid = UUID.fromString(aggregateId)
    val hashcode = uuid.hashCode()
    val partitionId = math.abs(hashcode) % totalPartitions
    partitionId
  }

  def serializeEventToMessage(event: Evt, eventName: String): String ={
    implicit val formats = DefaultFormats
    val jsonString = write(event)
    val message = eventName + ":::" + jsonString
    message
  }

  def sendMessageToKafkaTopic(event: Evt, aggregateId: String){
    val eventName = event.getClass.getSimpleName
    logger.debug("[KAFKA-PRODUCER]::::::::::::EVENT NAME: " + eventName)
    logger.debug("[KAFKA-PRODUCER]::::::::::::AGGREGATE ID: " + aggregateId)
    logger.debug("[KAFKA-PRODUCER]::::::::::::TOPIC NAME: " + topicName)

    val message = serializeEventToMessage(event, eventName)
    logger.info("[KAFKA-PRODUCER]::::::::::::MESSAGE FOR KAFKA TOPIC: " + message)

    val timestamp = System.currentTimeMillis()
    logger.debug("[KAFKA-PRODUCER]::::::::::::CURRENT TIMESTAMP: " + timestamp)

    val partitionId = calculatePartitionIdByAggregateIdAndTotalPartitions(aggregateId, topicPartitionsNumber)
    logger.debug("[KAFKA-PRODUCER]::::::::::::PARTITION ID: " + partitionId)

    val eventRecord = new ProducerRecord[String, String ](topicName, partitionId, timestamp,  aggregateId, message)

    try{
      kafkaProducer.send(eventRecord)
    } catch{

      case exception: Throwable => logger.error("[KAFKA-PRODUCER][ERROR] sending event to KAFKA JOURNAL : " + exception.getMessage )
        exception.printStackTrace()
    }
  }

}
