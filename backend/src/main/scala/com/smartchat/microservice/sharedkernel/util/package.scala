package com.smartchat.microservice.sharedkernel


/**
  * Created by xavier on 12/15/16.
  */
package object util {

  case class Ok()

  case class Error(message: String)

  case class Warning(message: String)

}
