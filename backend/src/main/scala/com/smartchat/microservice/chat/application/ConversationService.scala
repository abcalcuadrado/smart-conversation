package com.smartchat.microservice.chat.application

import akka.actor.{Actor, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.smartchat.microservice.chat.application.ConversationService.HandleFacebookMessage
import com.smartchat.microservice.chat.domain.aggregate.Conversation
import com.smartchat.microservice.chat.domain.aggregate.Conversation.{AppendBuyerMessageCmd, CreateConversationCmd}
import com.smartchat.microservice.chat.domain.manager.ConversationManager
import com.smartchat.microservice.chat.domain.repository.ConversationRepository
import com.smartchat.microservice.sharedkernel.domain.aggregate.AggregateRoot.IsCreatedCmd
import com.smartchat.microservice.sharedkernel.domain.manager.AggregateManager
import com.smartchat.microservice.sharedkernel.domain.repository.AggregateRepository.FindById
import scaldi.Injector
import scaldi.akka.AkkaInjectable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

/**
  * Created by xavier on 12/14/16.
  */

object ConversationService {

  case class HandleFacebookMessage(buyerId: String, sellerId: String, message: String, timestamp: Long)

}

class ConversationService(implicit inj: Injector) extends Actor with AkkaInjectable {

  implicit val timeout = Timeout(1 seconds)
  val conversationRepository = injectActorRef[ConversationRepository]
  val conversationManager = injectActorRef[ConversationManager]


  override def receive: Receive = {
    case HandleFacebookMessage(buyerId, sellerId, message, timestamp) =>
      val id = s"$sellerId~$buyerId"
      (conversationRepository ? FindById(id)).flatMap {
        case Some(actor) =>
          Future(actor)
        case None =>
          (conversationManager ? id) map {
            case newConversation: ActorRef =>
              val sellerAvatar = "http://static.giantbomb.com/uploads/original/0/4024/570673-apu_nahasapeemapetilon.png"
              val buyerAvatar = "http://orig15.deviantart.net/ae75/f/2011/036/7/9/homer_simpson___06___simpsons_by_frasier_and_niles-d38uqts.jpg"
              newConversation ! CreateConversationCmd(buyerId, buyerAvatar, sellerId, sellerAvatar)
              newConversation
          }

      }.map {
        case conversation: ActorRef => conversation.asInstanceOf[ActorRef] ! AppendBuyerMessageCmd(message, timestamp)
      }

    case _ =>

      sender() ? "--------->"
    //Delay para esperar a que el vendedor responda
    //Consultar Intent


  }
}
