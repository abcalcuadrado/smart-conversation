package com.smartchat.microservice.chat.rest

import com.smartchat.microservice.chat.rest.v1.{FacebookMessageWrapper, FacebookWebhookController, WebhookController}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import scaldi.Injector
import scaldi.akka.AkkaInjectable


/**
  * Created by xavier on 12/13/16.
  */
class MainController(implicit inj: Injector) extends AkkaInjectable {
  val route: Route = pathPrefix("chat") {
    pathPrefix("v1") {
      inject[WebhookController].route
    }
  }

}
