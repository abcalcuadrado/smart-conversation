package com.smartchat.microservice.chat.domain.repository

import com.smartchat.microservice.chat.domain.manager.ConversationManager
import com.smartchat.microservice.sharedkernel.domain.repository.AggregateRepository

/**
  * Created by xavier on 12/20/16.
  */


class ConversationRepository extends AggregateRepository[ConversationManager]
