package com.smartchat.microservice.chat

import com.smartchat.microservice.chat.application.ConversationService
import com.smartchat.microservice.chat.domain.manager.ConversationManager
import com.smartchat.microservice.chat.domain.repository.ConversationRepository
import com.smartchat.microservice.chat.rest.MainController
import com.smartchat.microservice.chat.rest.v1.{FacebookWebhookController, WebhookController}
import scaldi.{Injectable, Module}

/**
  * Created by xavier on 12/20/16.
  */
class DIModule extends Module with Injectable {

  binding toProvider new ConversationService
  binding toProvider new ConversationRepository
  binding toProvider new ConversationManager
  binding to new MainController
  binding to new FacebookWebhookController
  binding to new WebhookController

}
