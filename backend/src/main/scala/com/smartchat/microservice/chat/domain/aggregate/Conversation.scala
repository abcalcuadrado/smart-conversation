package com.smartchat.microservice.chat.domain.aggregate

import akka.actor.{Cancellable, Props, Scheduler}
import com.smartchat.microservice.chat.domain.aggregate.Conversation._
import com.smartchat.microservice.sharedkernel.domain.aggregate.AggregateRoot
import com.smartchat.microservice.sharedkernel.domain.aggregate.AggregateRoot.{Evt, IsCreatedCmd}
import com.smartchat.microservice.sharedkernel.util.{Error, Ok}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
  * Created by xavier on 12/14/16.
  */

object Conversation {
  def props(scheduler: Scheduler)(id: String) = Props(new Conversation(id, scheduler))


  case class AppendBuyerMessageCmd(message: String, timestamp: Long)

  case class AppendSellerMessageCmd(message: String, timestamp: Long)

  case class BecomeLastChanceCmd()

  case class BlockSellerCmd()


  case class CreateConversationCmd(buyerId: String, buyerAvatar: String, sellerId: String, sellerAvatar: String)


  case class ConversationCreatedEvt(buyer: User, seller: User, createdAt: Long, updatedAt: Long) extends Evt

  case class MessageAppendedEvt() extends Evt

  class ConversationState(buyerId: String, sellerId: String, createdAt: Long, updatedAt: Long)

  case class User(id: String, avatar: String)

  val SELLER_MESSAGES_NOT_ALLOW = "SELLER_MESSAGES_NOT_ALLOW"
  val NOT_CREATED_YET = "NOT_CREATED_YET"

}

class Conversation(id: String, scheduler: Scheduler) extends AggregateRoot {

  var state: Option[ConversationState] = None

  var cancellableOption: Option[Cancellable] = None

  //Deberia tener un last intent


  def handleSenderMessage(senderMessage: AppendBuyerMessageCmd): Unit = {
    cancellableOption match {
      case Some(cancellable) =>
        cancellable.cancel()
      case None =>
    }
    cancellableOption = Some(scheduler.scheduleOnce(1.day, self, BecomeLastChanceCmd()))
    context.become(sellerCountdown)
  }

  def notCreatedYet: Receive = {
    case IsCreatedCmd() =>
      sender() ! false
    case CreateConversationCmd(buyerId, buyerAvatar, sellerId, sellerAvatar) =>
      persist(ConversationCreatedEvt(User(buyerId, buyerAvatar),
        User(sellerId, sellerAvatar),
        System.currentTimeMillis(),
        System.currentTimeMillis()))(updateState)
      context.become(waitingForBuyer)

    case _ => sender() ! Error(NOT_CREATED_YET)
  }

  def waitingForBuyer: Receive = {
    case IsCreatedCmd() => sender() ! true
    case senderMessage: AppendBuyerMessageCmd =>
      handleSenderMessage(senderMessage)
      sender() ! Ok()


    case _ => sender() ! Error(SELLER_MESSAGES_NOT_ALLOW)
  }

  def sellerCountdown: Receive = {
    case buyerMessage: AppendBuyerMessageCmd =>
      handleSenderMessage(buyerMessage)
    case AppendSellerMessageCmd(message, timestamp) =>
      sender() ! Ok()
    case IsCreatedCmd => sender() ! true
    case BecomeLastChanceCmd() =>
      context.become(sellerLastChance)

  }

  def sellerLastChance: Receive = {

    case AppendBuyerMessageCmd(message, timestamp) =>
      context.become(sellerCountdown)

    case AppendSellerMessageCmd(message, timestamp) =>
      context.become(waitingForBuyer)
      sender() ! Ok()

    case IsCreatedCmd() => sender() ! true
  }

  override def receiveCommand: Receive = notCreatedYet

  def updateState(event: Evt): Unit =
    state = state

  override def persistenceId: String = id

  override def receiveRecover: Receive = {
    case _ =>
  }
}
