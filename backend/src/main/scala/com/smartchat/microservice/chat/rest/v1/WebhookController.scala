package com.smartchat.microservice.chat.rest.v1

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import scaldi.Injector
import scaldi.akka.AkkaInjectable

/**
  * Created by xavier on 12/13/16.
  */
class WebhookController(implicit inj: Injector) extends AkkaInjectable {

  val route: Route = pathPrefix("webhooks") {
    inject[FacebookWebhookController].route
  }
}
