package com.smartchat.microservice.chat.rest.v1

import akka.actor.ActorSystem
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import com.smartchat.microservice.chat.application.ConversationService
import scaldi.Injector
import scaldi.akka.AkkaInjectable
import spray.json.DefaultJsonProtocol
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import com.smartchat.microservice.chat.application.ConversationService.HandleFacebookMessage

import scala.concurrent.duration._
import scala.concurrent.Future

case class User(id: String)

case class Message(mid: String, seq: Int, text: String)

case class Messaging(sender: User, recipient: User, timestamp: Long, message: Message)

case class Entry(id: String, time: Long, messaging: Seq[Messaging])

case class FacebookMessageWrapper(`object`: String, entry: Seq[Entry])

class FacebookWebhookController(implicit inj: Injector) extends AkkaInjectable with DefaultJsonProtocol with SprayJsonSupport {

  implicit val userFormatter = jsonFormat1(User)
  implicit val messageFormatter = jsonFormat3(Message)
  implicit val messagingFormatter = jsonFormat4(Messaging)
  implicit val entryFormatter = jsonFormat3(Entry)
  implicit val facebookMessageWrapperFormatter = jsonFormat2(FacebookMessageWrapper)
  implicit val timeout = Timeout(1 seconds)
  implicit val system = inject[ActorSystem]

  val route: Route = path("facebook") {
    post {
      entity(as[FacebookMessageWrapper]) {
        facebookMessageWrapper => {
          val message = facebookMessageWrapper.entry.head.messaging.head
          injectActorRef[ConversationService] ? HandleFacebookMessage(message.sender.id, message.recipient.id, message.message.text, message.timestamp)
          complete("hola que ase")
        }
      }
    }
  }

}
