package com.smartchat.microservice.chat.domain.manager

import akka.actor.Props
import com.smartchat.microservice.chat.domain.aggregate.Conversation
import com.smartchat.microservice.sharedkernel.domain.manager.AggregateManager

/**
  * Created by xavier on 12/21/16.
  */
class ConversationManager extends AggregateManager {
  override val propsFactory: (String) => Props = Conversation.props(context.system.scheduler)
}
